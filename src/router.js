import Vue from "vue";
import Router from "vue-router";
import Dashboard from "@/components/views/Dashboard";
import TierConfiguration from "@/components/views/TierConfiguration";
import Allocation from "@/components/views/Allocation";
import Vendors from "@/components/views/Vendors";
import History from "@/components/views/History";
import strings from "../src/utils/strings"

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Dashboard //need to pick a default view
    },
    {
      path: strings.APP_ROUTES.DASHBOARD_PATH,
      name: strings.APP_ROUTES.DASHBOARD_NAME,
      component: Dashboard
    },
    {
        path: strings.APP_ROUTES.TIERS_PATH,
        name: strings.APP_ROUTES.TIERS_NAME,
        component: TierConfiguration
    },
    {
        path: strings.APP_ROUTES.ALLOCATION_PATH,
        name: strings.APP_ROUTES.ALLOCATION_NAME,
        component: Allocation
    },
    {
      path: strings.APP_ROUTES.VENDOR_PATH,
      name: strings.APP_ROUTES.VENDOR_NAME,
      component: Vendors
  },
    {
      path: strings.APP_ROUTES.HISTORY_PATH,
      name: strings.APP_ROUTES.HISTORY_NAME,
      component: History
  }
  ]
}); 
