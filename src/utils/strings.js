//vue components need to be exported as default, so this strings file is just for storing consts that could have otherwise gone
//into their respective components
export default {
  //used in Top.vue, LeftSide.vue, router.js
  APP_ROUTES: {
    ALLOCATION_PATH: '/allocation',
    ALLOCATION_NAME: 'Allocation',
    DASHBOARD_PATH: '/dashboard',
    DASHBOARD_NAME: 'Dashboard',
    HISTORY_PATH: '/history',
    HISTORY_NAME: 'History',
    TIERS_PATH: '/tiers',
    TIERS_NAME: 'Tiers',
    VENDOR_PATH: '/vendors',
    VENDOR_NAME: 'Vendors',
  },
  //AllocationCard.vue, Allocation.vue
  ALLOCATION_CARD_TYPE: {
    STATE: "state",
    COUNTY: "county",
    ZIP: "zip",
    NATION: "nation",
    TOTAL: "total"
  },
  ALLOCATIONS_LOADED: "allocsLoaded",
  //AllocationCard.vue
  ENTITY_TYPE: {
    FORWARDER: "F",
    VENDOR: "V",
    getLongType(type) {
      switch (type) {
        case this.FORWARDER:
          return "Forwarder";
        case this.VENDOR:
          return "Vendor";
        default:
          return "UNK";
      }
    }
  }
};
