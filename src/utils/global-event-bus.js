import Vue from "vue";
//Global Event Bus
/*
The event bus / publish-subscribe pattern, despite the bad press it sometimes gets,
is still an excellent way of getting unrelated sections of your application to talk to each other.
https://alligator.io/vuejs/global-event-bus/
*/
export const GlobalEventBus = new Vue();

 //Current Global Events
export const GLOBAL_EVENTS = {
    TOP_BAR_SET_SEARCH_VISIBILITY: "top_bar_set_search_visibility", //allow views to hide/show search bar if view implements search_changed event
    TOP_BAR_SEARCH_CHANGED: "top_bar_search_changed", //emits every time user types into top search input
    TOP_BAR_SEARCH_ENTER_PRESSED: "top_bar_search_enter_pressed", //emits when user hits enter on search
    TOP_BAR_SET_SUBLINK_VISIBILITY: "top_bar_set_sublink_visibility", //allow views to hide/show sublinks, toggles submenu visibility if turned on
    TOP_BAR_SET_CUSTOM_SUBLINKS: "top_bar_set_custom_sublinks", //pass in your own links
    TOP_BAR_RESET_TIERING_SUBLINKS: "top_bar_reset_tiering_sublinks", //defaults back to tiering sublinks
    TOP_BAR_SET_SUBMENU_ITEMS_VISIBILITY: "top_bar_set_submenu_items_visiblity", //set visible for submenu items, toggles sublink visibility if turned on
    TOP_BAR_SET_SUBMENU_ITEMS: "top_bar_set_submenu_items", //pass in items with click callbacks to be used by your view
    TOP_BAR_RESET_SUBMENU_ITEMS: "top_bar_reset_submenu_items", //sets submenu back to empty
    TOP_BAR_RESET_AUTOCOMPLETE_ITEMS: "top_bar_reset_autocomplete_items", //removes autocomplete,function as reegular input
    TOP_BAR_SET_AUTOCOMPLETE_ITEMS: "top_bar_set_autocomplete_items", //send an itemsource to top searchbar
    TOP_BAR_SET_AUTOCOMPLETE_STATES: "top_bar_set_autocomplete_states", //special autocomplete itemsource for states
    TOP_BAR_SET_AUTOCOMPLETE_COUNTIES: "top_bar_set_autocomplete_counties", //special autocomplete itemsource for counties
    TOP_BAR_SET_AUTOCOMPLETE_STATES_COUNTIES: "top_bar_set_autocomplete_counties", //special autocomplete itemsource for merged
    TOP_BAR_AUTOCOMPLETE_ITEM_SELECTED: "top_bar_autocomplete_item_selected", //user has clicked or hit enter on an item
    TOP_BAR_SET_LOADING: "top_bar_set_loading", //send loading true/false to the top bar
    TOP_BAR_HELP_ICON_CLICKED: "top_bar_help_icon_clicked", //send loading true/false to the top bar
    TOP_BAR_SET_TITLE: "top_bar_set_title", //set the top bar title
    LEFT_SIDEBAR_TOGGLE: "left_sidebar_toggle", //toggles visibility of the Left Sidebar
    LEFT_SIDEBAR_CLOSE: "left_sidebar_close", //force closes the Left Sidebar
    LEFT_SIDEBAR_OPEN: "left_sidebar_open", //force opens the Left Sidebar
    ALLOCATION_CARD_UPDATED: "allocation_card_updated", //emitted by allocation cards on update with the card info
}

/*
you can access global events by using GlobalEventBus.$events.LEFT_SIDE_TOGGLE...
but there is no autocomplete.

if you use:
import { GLOBALEVENTS } from "../../utils/global-event-bus";
then you can have GLOBAL_EVENTS.LEFT_SIDE_TOGGLE with autocomplete...
*/
Vue.prototype.$events = GLOBAL_EVENTS;
