/*https://vuejs.org/v2/guide/mixins.html
    this is a mixin of common code used by Allocation/Dashboard view components
*/

import strings from "../utils/strings"
import { mapGetters, mapState, mapMutations } from "vuex"
import { SET_TIER_NAME, SET_TIER_TRANSITION, ADD_TIER, COPY_TIER, REMOVE_TIER, REMOVE_VENDOR } from "../store/mutations"
import { GlobalEventBus, GLOBAL_EVENTS } from "../utils/global-event-bus";
import ModalRemoveTier from '../components/modals/RemoveTier'
import ModalReorderTier from '../components/modals/ReorderTier'
import ModalCopyTier from '../components/modals/CopyTier'

export default {
  data() {
    return {
      activeTier: {},
      activeTierName: '', //due to tiers not having an ID - we use the name for ID, so this property is for 1 way binding
      currentConfig: {},
      tiers: [{}],
      tierChangeKey: -1, //https://michaelnthiessen.com/force-re-render/
      nationCard: { name:"United States", abbreviation: "USA", activeCard: true, type: strings.ALLOCATION_CARD_TYPE.NATION },
      placeholderCounties: "Counties",
      placeholderZips: "County Zip Codes",
      zipFilter: "",
      zipCodeSearch: false,
      zipSearchFilter: "",
      zipSearchCards:[],
      zipCards:[],
      tierLoading: false,
    }
  },
  computed: {
    ...mapState({
      _states: state => state.geo.states,
      _isDirty: state => state.tiers.isDirty
    }),
    ...mapGetters("geo", {
      countiesByStateAbbr: "countiesByStateAbbr",
      zipsByCountyStateAbbr: "zipsByCountyStateAbbr"
    }),
    ...mapGetters("tiers", {
      getAllocationRule: "getAllocationRule",
      getCurrentConfig: "createCurrentConfigLocalCopy",
      getZipCodeRules: "getZipCodeRules",
      getVendorAllocations: "getVendorAllocations"
    }),
    nationAllocation: {
      cache: false,
      get() {
        return this.getAllocationRule({ tier: this.activeTier, name: "United States", abbreviation:"USA", type: strings.ALLOCATION_CARD_TYPE.NATION }).allocations;
      }
    },
    filteredZipCodes: {
      cache: false,
      get(){
        return this.zipCards.filter(zip => zip.name.toLowerCase().includes(this.zipFilter.toLowerCase()));
      }
    },
    filteredZipSearches(){
      if (this.zipSearchCards) {
        let zc = this.zipSearchCards.filter(zip =>
          zip.name.toLowerCase().includes(this.zipSearchFilter.toLowerCase())
        );
        if (this.zipSearchFilter.length ===  5 &&  Number(this.zipSearchFilter) && zc.length === 0){
          this.insertZipSearchCard(); //return after inserting otherwise computed value doesn't refilter
          return this.zipSearchCards.filter(zip => zip.name.toLowerCase().includes(this.zipSearchFilter.toLowerCase())
        );
        }
        return zc;
      }
      return [];
    },
    activeTierDuration: {
      cache: false,
      get() {
        return this.activeTier?.transition_criteria?.time?.days;
      },
      set(val) {
        if (this.activeTier?.transition_criteria?.case_extra?.post_charge_off) {
          delete this.activeTier.transition_criteria.case_extra.post_charge_off;
        }
        if (this.activeTier.transition_criteria) {
          this.activeTier.transition_criteria.time = {
            days: val
          };
        } else {
          this.activeTier.transition_criteria = {
            time: { days: val }
          };
        }
        this.commitTierTransition({ tier: this.activeTier });
      }
    },
    activeTierPostChargeOff: {
      cache: false,
      get() {
        return this.activeTier?.transition_criteria?.case_extra
          ?.post_charge_off;
      },
      set(val) {
        if (val) {
          //go through the rest of the tiers and remove their post_charge_off flags if present on any of them and alert the user
          this.tiers.forEach(t => {
            if (t?.transition_criteria?.case_extra?.post_charge_off === true) {
              this.$toasted.show(
                `</br>Post charge off was previously set in the (${t.ordinal})-${t.name} tier.</br></br>You'll need to set a duration for ${t.name} in order to save your changes.</br></br>`,
                { duration: 12000, type: "success" }
              );
              delete t.transition_criteria;
              this.commitTierTransition({ tier: t });
            }
          });
        }
        //do a bunch of checks to not overwrite any additional case_extra values
        if (
          this.activeTier.transition_criteria &&
          this.activeTier.transition_criteria.case_extra
        ) {
          this.activeTier.transition_criteria.case_extra.post_charge_off = val;
        } else if (this.activeTier.transition_criteria) {
          this.activeTier.transition_criteria.case_extra = {
            post_charge_off: val
          };
        } else {
          this.activeTier.transition_criteria = {
            case_extra: {
              post_charge_off: val
            }
          };
        }
        if(!val){ //if false, completely remove the property
          delete this.activeTier.transition_criteria.case_extra;
        }
        this.commitTierTransition({ tier: this.activeTier });
        this.$forceUpdate();
      }
    }
  },
  mounted(){

    //if we want to disable zip code search in the mapview later, move this code to the mounted function in Allocation.vue
    GlobalEventBus.$on(GLOBAL_EVENTS.TOP_BAR_SEARCH_ENTER_PRESSED, text => {
      let zip = Number(text);
      if (zip && text.length === 5){
        this.zipCodeSearch = true;         
        this.zipSearchFilter = text;
      } else if (zip){
        this.$toasted.show(`Please enter a 5 digit zip code and then press enter again.`, { duration : 3000, type: 'error' });
      }
    });

    GlobalEventBus.$emit(GLOBAL_EVENTS.TOP_BAR_SET_AUTOCOMPLETE_STATES_COUNTIES);
    this.$store.dispatch("tiers/getTierConfiguration").then(() => {
      this.currentConfig = this.getCurrentConfig();
      this.tiers = this.currentConfig.tiers;     
      this.setTier(this.tiers.find(t => t.active === true) || this.tiers[0]);
      //this view will use the submenu space as action items
      this.refreshTopMenu();
    });
  },
  methods: {
    ...mapMutations({
      commitTierName: 'tiers/'+SET_TIER_NAME,
      commitTierTransition: 'tiers/'+SET_TIER_TRANSITION,
      commitAddTier: 'tiers/'+ADD_TIER,
      commitCopyTier: 'tiers/'+COPY_TIER,
      commitRemoveTier: 'tiers/'+REMOVE_TIER,
      commitRemoveVendor: 'tiers/'+REMOVE_VENDOR
    }),
    refreshTopMenu(){
      let menu = this.tiers.sort((a, b) => a.ordinal - b.ordinal).map((t, index) => {
        return { menuTitle: "Tier " + (index+1), ordinal: t.ordinal, active: t.name === this.activeTier.name, callback: this.submenuCallback, name: t.name }
      })
      GlobalEventBus.$emit(GLOBAL_EVENTS.TOP_BAR_SET_SUBMENU_ITEMS, [ ...menu, 
      { menuTitle: "Add Tier", name:"Add a new tier", active: false, callback: this.addTier },
      { menuTitle: "Copy Tier", name:"Copy a tier with its allocations", active: false, callback: this.copyTier },
      { menuTitle: "Remove Tier", name:"Remove a tier", active: false, callback: this.removeTier }]);
      GlobalEventBus.$emit(GLOBAL_EVENTS.TOP_BAR_SET_SUBMENU_ITEMS_VISIBILITY, true);
    },
    submenuCallback(item) {
      this.setTier(this.tiers.find(t => t.name === item.name));
    },
    insertZipSearchCard(){
      this.zipSearchCards.push({ tier: this.activeTier, name: this.zipSearchFilter, activeCard: false, type: strings.ALLOCATION_CARD_TYPE.ZIP });
    },
    pcoChanged(){
      if (this.activeTier?.transition_criteria?.case_extra?.post_charge_off){
        this.$toasted.show(`${this.activeTier.name} set to post charge-off.`, { duration : 3000, type: 'info' });
      } else {
        this.$toasted.show(`${this.activeTier.name} set to pre charge-off.`, { duration : 3000, type: 'info' });
      }
    },
    removeTier(){
      this.$modal.show(ModalRemoveTier, {items: [...this.tiers]}, {/*optional style*/}, {
        'remove-tier': item => {
          this.commitRemoveTier(item.name);
          this.tiers.splice(this.tiers.findIndex(t=>t.name === item.name),1);
          this.tiers.forEach((item, idx) => item.ordinal = idx+1);
          //active tier is being removed, so automatically set the first tier so UI reloads
          if(this.activeTier.name === item.name && this.tiers.length > 0){
            this.setTier(this.tiers[0]);
          } else if (this.tiers.length === 0) {
            //automatically initialize empty tier1
            this.addTier();
          }
          this.refreshTopMenu();
        }
      });
    },
    copyTier(){
      this.$modal.show(ModalCopyTier, {items: [...this.tiers]}, {/*optional style*/}, {
        'copy-tier': item => {
          this.addTier(item, true);
        }
      });
    },
    addTier(item, copy) {
      this.tiers.forEach(tier => tier.active = false);
      let tier = { menuTitle: "Tier " + (this.tiers.length+1), ordinal: this.tiers.length+1, active: true, callback: this.submenuCallback, name: "tier"+(this.tiers.length+1) };
      if(copy){
        tier.name = 'copy-'+item.name;
        this.commitCopyTier({copy:item.name, tier: { ordinal: this.tiers.length+1, name:tier.name, allocation_weights: {}, transition_criteria:{}}})
      } else {
        this.commitAddTier({ ordinal: this.tiers.length+1, name: "tier"+(this.tiers.length+1), allocation_weights: {}, transition_criteria:{} });
      }
      this.tiers.push(tier);
      this.setTier(tier);
      this.refreshTopMenu();
    },
    reorderTiers() {
      this.$modal.show(ModalReorderTier, {items: [...this.tiers]}, {/*optional style*/}, {
        'reorder-tier': (tiers) => {
          this.tiers = [...tiers];
          this.tiers.forEach((tier) => tier.active = this.activeTier.name === tier.name ? true : false);
          this.refreshTopMenu();
        }
      });
    },
    setTier(tier){
      //temporarily turn off pins so cards rerender and aren't hidden
      this.tierLoading = true;
      this.tiers.forEach(tier => tier.active = false);
      this.activeTier = tier;
      this.activeTier.active = true;
      this.activeTierName = this.activeTier.name;
      this.nationCard.tier = this.activeTier;
      this.tierChangeKey++;
      
      //when tier changes we need to reset the cards but only in allocation.vue, which has a this.stateCards data obj
      if(this.stateCards) {
        if(this.stateCards.length === 0){
          this._states.forEach(state => {
            this.stateCards.push({
              ...state,
              activeCard: false,
              tier: this.activeTier,
              type: strings.ALLOCATION_CARD_TYPE.STATE
            });
          });
        } else {
          this.stateCards.forEach(s => { s.tier = this.activeTier});
        }
        this.countyCards.forEach(c => { c.tier = this.activeTier});
      }
      //when tier changes, needed to reload layer properties on dashboard.vue, which has a this.geojson data obj
      if(this.geojson) {
        this.reloadMapCardLayerData();
      }

      //need to redraw granular rules if a vendor is active selected on vendors.vue
      if(this.activeVendor){
        this.activeVendor.activeCard = false;
        this.vendorAssignedStateCards = [];
        this.vendorAssignedCountyCards = [];
        this.vendorAssignedZipCards = [];
        this.selectVendor(this.activeVendor);
      }
      
      //load the granular zipcode rules
      this.zipCards.forEach(z => { z.tier = this.activeTier});
      this.zipSearchCards = this.getZipCodeRules({tier:this.activeTier});
      //hack to force rerender for pinned cards
      setTimeout(() => {
        this.tierLoading = false;
      },1);
    },
    updateTierName(){
      if(this.activeTierName.trim().length > 0){
        //check the tier names and prevent dupes
        if(this.tiers.findIndex(t=> t.name.toLowerCase() === this.activeTierName.toLowerCase()) > -1 && this.activeTierName !== this.activeTier.name){
          this.$toasted.show(`Tier cannot be named ${this.activeTierName} because it is already in use.`, { duration : 3000, type: 'error' })
          return;
        }
        if(this.activeTierName !== this.activeTier.name){ //don't fire a save function if they end up editing the name to the same name as it was before
          this.commitTierName({ tier: this.activeTier, name: this.activeTierName });
          this.activeTier.name = this.activeTierName;
          this.refreshTopMenu();
        }
      } else {
        this.$toasted.show(`Tier name cannot be empty.`, { duration : 3000, type: 'error' });
      }
    },
    updateTierConfig(){
     this.$store.dispatch("tiers/saveNewTierConfiguration", JSON.stringify(this.getCurrentConfig())).then(() => {
        this.$toasted.show(`Changes will apply to new cases.`, { duration : 8000, type: 'success' });
      }, () => {
        this.$toasted.show(`Please contact Recovery Database Network support with any error messages shown for assistance.`, { duration : 8000, type: 'error' });
      }); //if this promise fails... nothing to really do, there will be a toast that tells them it couldn't save
    },
  },
  destroyed(){
    //reset the top bar nav links, searches, and search since we're leaving this page
    GlobalEventBus.$emit(GLOBAL_EVENTS.TOP_BAR_RESET_AUTOCOMPLETE_ITEMS);
    GlobalEventBus.$emit(GLOBAL_EVENTS.TOP_BAR_RESET_TIERING_SUBLINKS);
    GlobalEventBus.$emit(GLOBAL_EVENTS.TOP_BAR_SET_SUBLINK_VISIBILITY, true);
    GlobalEventBus.$emit(GLOBAL_EVENTS.TOP_BAR_SET_SEARCH_VISIBILITY, true);
    GlobalEventBus.$off(GLOBAL_EVENTS.TOP_BAR_SEARCH_ENTER_PRESSED);
    GlobalEventBus.$off(GLOBAL_EVENTS.TOP_BAR_HELP_ICON_CLICKED);
  }
};
