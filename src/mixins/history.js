/*https://vuejs.org/v2/guide/mixins.html
    this is a mixin of common code used by Allocation/Dashboard view components
*/

import { mapState, mapGetters } from "vuex";
import Avatar from "vue-avatar";

export default {
  components: {
    Avatar
  },
  data() {
    return {
      logs: [],
      tierConfigs: [{}],
      //mock history
      history: [
        { 
          id: 1, 
          title: "Tier 1 - First Placement",
          role: "Administrator",
          description: "Changes: Clark County, NV -> Zane Investigations: 50%, Alpha Recovery: 50%",
          name: "Nhan Diep",
          date: "Jan 12 2020",
          img: "https://static.intercomassets.com/avatars/1185248/square_128/profile-1493250643.jpg?1493250643"
        },
        { 
          id: 2, 
          title: "Tier 2 - Second Placement",
          role: "Administrator",
          description: "Changes: Miami-Dade County, FL -> Specialized Towing: 50%, Rapid Recovery: 50%",
          name: "Ryan Nadeau",
          date: "Jan 04 2020",
          img: "https://static.intercomassets.com/avatars/251144/square_128/ryan-1462427895-1462480826.jpg?1462480826"
        },
        { 
          id: 3, 
          title: "Tier 4 - Heavy Skip",
          role: "Administrator",
          description: "Changes: New Mexico -> ARS: 70%, SCM: 30%",
          name: "Justin Zane",
          date: "Dec 28 2019",
          img: "https://static.intercomassets.com/avatars/47542/square_128/justin-zane-1462404664-1462456851.png?1462456851"
        },
        { 
          id: 4, 
          title: "Tier 5 - Deep Skip",
          role: "Administrator",
          description: "Removed Tier",
          name: "John Sullivan",
          date: "Dec 19 2019",
          img: "https://static.intercomassets.com/avatars/47457/square_128/john-sullivan-profile-pic-1471469437.png?1471469437"
        }
      ],
    };
  },
  computed: {
    ...mapState({
      _tierConfigs: state => state.tiers.configs
    }),
    ...mapGetters("tiers", {
      getVendorByUdbsaid: "getVendorByUdbsaid"
    })
  },
  methods: {
    getNameForPath(p) {
      switch (p) {
        case "transition_criteria":
          return "Tier Transition";
        case "time":
          return "Duration";
        case "allocation_weights":
          return "Allocation";
        case "national":
          return "Nation";
        case "name":
          return "Name";
        case "days":
          return "Days";
        case "case_extra":
          return "System";
        case "post_charge_off":
          return "Post Charge Off";
        case "postal_codes":
          return "Zip Code";
        case "states":
          return "States";
        case "state":
          return "State";
        case "counties":
          return "County";
      }
      if (p.length > 5) {
        //likely a udbsaid
        var v = this.getVendorByUdbsaid(p);
        if (v) {
          return v.name;
        }
      }
      return p; //likely a zip code or new property
    }
  }
};
