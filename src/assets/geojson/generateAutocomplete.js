//🥶 this is just a nodejs script to generate the autocomplete itemsource for geo search 🥶
var fs = require('fs'); 
console.log("GENERATING counties.json & states.json");
const _states = [{"name":"Alabama","abbreviation":"AL"},{"name":"Alaska","abbreviation":"AK"},{"name":"Arizona","abbreviation":"AZ"},{"name":"Arkansas","abbreviation":"AR"},{"name":"California","abbreviation":"CA"},{"name":"Colorado","abbreviation":"CO"},{"name":"Connecticut","abbreviation":"CT"},{"name":"Delaware","abbreviation":"DE"},{"name":"District Of Columbia","abbreviation":"DC"},{"name":"Florida","abbreviation":"FL"},{"name":"Georgia","abbreviation":"GA"},{"name":"Hawaii","abbreviation":"HI"},{"name":"Idaho","abbreviation":"ID"},{"name":"Illinois","abbreviation":"IL"},{"name":"Indiana","abbreviation":"IN"},{"name":"Iowa","abbreviation":"IA"},{"name":"Kansas","abbreviation":"KS"},{"name":"Kentucky","abbreviation":"KY"},{"name":"Louisiana","abbreviation":"LA"},{"name":"Maine","abbreviation":"ME"},{"name":"Maryland","abbreviation":"MD"},{"name":"Massachusetts","abbreviation":"MA"},{"name":"Michigan","abbreviation":"MI"},{"name":"Minnesota","abbreviation":"MN"},{"name":"Mississippi","abbreviation":"MS"},{"name":"Missouri","abbreviation":"MO"},{"name":"Montana","abbreviation":"MT"},{"name":"Nebraska","abbreviation":"NE"},{"name":"Nevada","abbreviation":"NV"},{"name":"New Hampshire","abbreviation":"NH"},{"name":"New Jersey","abbreviation":"NJ"},{"name":"New Mexico","abbreviation":"NM"},{"name":"New York","abbreviation":"NY"},{"name":"North Carolina","abbreviation":"NC"},{"name":"North Dakota","abbreviation":"ND"},{"name":"Ohio","abbreviation":"OH"},{"name":"Oklahoma","abbreviation":"OK"},{"name":"Oregon","abbreviation":"OR"},{"name":"Palau","abbreviation":"PW"},{"name":"Pennsylvania","abbreviation":"PA"},{"name":"Puerto Rico","abbreviation":"PR"},{"name":"Rhode Island","abbreviation":"RI"},{"name":"South Carolina","abbreviation":"SC"},{"name":"South Dakota","abbreviation":"SD"},{"name":"Tennessee","abbreviation":"TN"},{"name":"Texas","abbreviation":"TX"},{"name":"Utah","abbreviation":"UT"},{"name":"Vermont","abbreviation":"VT"},{"name":"Virginia","abbreviation":"VA"},{"name":"Washington","abbreviation":"WA"},{"name":"West Virginia","abbreviation":"WV"},{"name":"Wisconsin","abbreviation":"WI"},{"name":"Wyoming","abbreviation":"WY"}];
var states = JSON.parse(fs.readFileSync('states_small.json', 'utf8'));
var statesJson = [];
var stateMapping = {};

for (var i = 0; i < states.features.length; i++){
    var state = states.features[i];
    let abbr = _states.find(s => s.name.toLowerCase() === state.properties.NAME.toLowerCase()).abbreviation || "";
    stateMapping[state.properties.STATE] = {
        name: state.properties.NAME,
        abbreviation: abbr,
    };
    if(state.geometry.coordinates[0][0].length === 2){
        state.geometry.coordinates[0][0].reverse();
        statesJson.push({ name: state.properties.NAME, abbreviation: abbr, state: state.properties.STATE, lat: state.geometry.coordinates[0][0][0], lng: state.geometry.coordinates[0][0][1] });
    } else {
        state.geometry.coordinates[0][0][0].reverse();
        statesJson.push({ name: state.properties.NAME, abbreviation: abbr, state: state.properties.STATE, lat: state.geometry.coordinates[0][0][0][0], lng: state.geometry.coordinates[0][0][0][1] });
    }  
}

var counties = JSON.parse(fs.readFileSync('counties_small.json', 'utf8'));
var countiesJson = [];

for (var i = 0; i < counties.features.length; i++){
    var county = counties.features[i];
    if(county.geometry.coordinates[0][0].length === 2){
        county.geometry.coordinates[0][0].reverse();
        countiesJson.push({ name: county.properties.NAME, state:county.properties.STATE, stateInfo: stateMapping[county.properties.STATE], lat: county.geometry.coordinates[0][0][0], lng: county.geometry.coordinates[0][0][1] });
    } else {
        county.geometry.coordinates[0][0][0].reverse();
        countiesJson.push({ name: county.properties.NAME, state:county.properties.STATE, stateInfo: stateMapping[county.properties.STATE], lat: county.geometry.coordinates[0][0][0][0], lng: county.geometry.coordinates[0][0][0][1] });
    }  
}

//console.log(statesJson);
//console.log(countiesJson);

fs.writeFileSync('counties.json', JSON.stringify(countiesJson), 'utf-8');
fs.writeFileSync('states.json', JSON.stringify(statesJson), 'utf-8');
fs.writeFileSync('state_mapping.json', JSON.stringify(stateMapping), 'utf-8');