//this app also uses https://babeljs.io/docs/en/babel-plugin-proposal-optional-chaining
import Vue from "vue";
import App from "./App.vue";
//side note: the router MUST BE called 'router' and not Router or anything else
import router from "./router";
import "./assets/css/tailwind.css";
import store from "./store";
import icons from "./assets/icons";

//https://github.com/shakee93/vue-toasted
import Toasted from "vue-toasted";

//https://github.com/paliari/v-autocomplete
//https://github.com/paliari/v-autocomplete#properties
import Autocomplete from "v-autocomplete";

//https://github.com/Akryum/v-tooltip
import VTooltip from "v-tooltip";

//https://github.com/euvl/vue-js-modal/
import VModal from "vue-js-modal";
//https://github.com/euvl/vue-js-toggle-button - I liked his modal so much I added his toggle btn
import ToggleButton from "vue-js-toggle-button";

//https://akryum.github.io/vue-progress-path/
import VueProgress from "vue-progress-path";

//https://github.com/brockpetrie/vue-moment
import VueMoment from "vue-moment";

//https://github.com/SortableJS/Vue.Draggable - this is the drag and drop lib

//https://eliep.github.io/vue-avatar/ - also using vue-avatar

//We have an aggressive lint - you can use the below comment after any line of code to disable the lint check for it
//eslint-disable-line

Vue.config.productionTip = false;
Vue.use(icons);
Vue.use(Toasted);
Vue.use(Autocomplete);
Vue.use(VTooltip);
Vue.use(VModal, {
  dynamic: true,
  dynamicDefaults: { clickToClose: true },
  dialog: true
});
Vue.use(ToggleButton);
Vue.use(VueProgress);
Vue.use(VueMoment);

/*
Global mixins should be declared in main.js, functions that can be used from any component - vue team says to use them sparingly
https://vuejs.org/v2/guide/mixins.html#Global-Mixin
*/
Vue.mixin({
  methods: {
    helloWorld() {
      alert("hello world");
    },
    navExternal(link) {
      //if vuejs app is using vuerouter, it does not allow nav outside of the app when used,
      //have to use window https://michaelnthiessen.com/redirect-in-vue
      console.log(link); //eslint-disable-line
      window.location.href = link;
    },
    toast(msg) {
      //quick informational/debug toast because I can never remember the syntax
      this.$toasted.show(msg, { duration: 2000 });
    },
    truncate(name, length) {
      length = length || 20;
      return name.length < length ? name : name.substring(0, length) + ".."; //save space, only 2 .. instead of 3
    },
    uuidv4() {
      return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(
        c
      ) {
        var r = (Math.random() * 16) | 0,
          v = c == "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
      });
    }
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
