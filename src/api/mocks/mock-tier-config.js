/**
 * Mocking client-server processing and tier configuration service
 * https://karauctionservices.visualstudio.com/RDN/_wiki/wikis/RDN.wiki/1948/Tier-Configuration-Service
 *
 * This can be used for local dev by simply replacing
 * import tc from '../../api/tier-config'
 * with
 * import tc from '../../api/mocks/mock-tier-config'
 *
 */
//update mock data here - Don't format this document or you'll get a ton of 1 lines zips, states, counties...
import _states from '../../store/modules/geodata/states.json'
import _counties from '../..//store/modules/geodata/counties.json'
import _tiers from '../mocks/mock-tier.json'
import _zips from '../mocks/mock-zips.json'
import _vendors from '../mocks/mock-vendors.json'
import _forwarders from '../mocks/mock-forwarders.json'
import _history from '../mocks/mock-history-list.json'
import _created from '../mocks/mock-history-created.json'
import _updated from '../mocks/mock-history-updated.json'

import axios from "axios";

//testing axios init
const cd = {
  api: axios.create({
    baseURL: "https://api.cleardata.io",
    headers: { "content-type": "application/json" },
    withCredentials: true
  }),
  URL_VERSION: "version"
};

cd.api.interceptors.response.use(response => {
  console.log('testing intercepted response', response);//eslint-disable-line
  return response;
}, error => {
  if (401 === error.response.status) {
    alert('Please login again.');
    window.location = process.env.VUE_APP_REDIRECT;
  } else {
      return Promise.reject(error);
  }
});

export default {
  /*
   * Sample Usage in App
   * import tc from '../../api/mocks/mock-tier-config'
   *
   * tc.getCounties({ state: "nv"}, counties => {
   *      console.log(counties); //eslint-disable-line
   *    }, err => {
   *      console.log(err); //eslint-disable-line
   * });
   *
   */
  getVersion(cb, errCb) {
    cd.api
      .get(cd.URL_VERSION)
      .then(response => {
        cb(response.data);
      })
      .catch(function(error) {
        errCb(error);
      });
  },
  //unlike the real service, none of these mocks use error callbacks - all successes
  getTierConfigurations(cb) {
    setTimeout(() => cb(_tiers), 50);
  },
  saveTierConfigurations(data, cb) {
    setTimeout(() => cb({}), 50);
  },
  updateTierConfigurations(id, data, cb) {
    setTimeout(() => cb({}), 50);
  },
  getCounties(params, cb) {
    //params: { state: "NV" }
    setTimeout(() => cb(_counties), 50);
  },
  getZipCodes(params, cb) {
    //params: { state: "nv", county: "washoe" } - county requires state
    setTimeout(() => cb(_zips), 50);
  },
  getStates(cb) {
    setTimeout(() => cb(_states), 50);
  },
  getVendors(params, cb) {
    //params: { postal_code:"89521", state: "nv", county: "washoe" }
    //postal code and state are standalone params, county requires state
    setTimeout(() => cb(_vendors), 50);
  },
  getForwarders(params, cb) {
    //params: { postal_code:"89521", state: "nv", county: "washoe" }
    //postal code and state are standalone params, county requires state
    setTimeout(() => cb(_forwarders), 50);
  },
  getHistory(cb) {
    setTimeout(() => cb(_history), 50);
  },
  getHistoryById(id, cb) {
    if(id === 1){
      setTimeout(() => cb(_created), 50);
    } else {
      setTimeout(() => cb(_updated), 50);
    }
  },
};
