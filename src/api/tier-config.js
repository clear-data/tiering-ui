/**
 * Tier Configuration Service
 * https://karauctionservices.visualstudio.com/RDN/_wiki/wikis/RDN.wiki/1948/Tier-Configuration-Service
 * for local development, can do port-forward to nonprod
 * kubectl port-forward svc/tier-configuration 4111:80
 */
import axios from "axios";

axios.interceptors.response.use(function (response) {
  console.log('do i get here', response);//eslint-disable-line
  return response;
}, function (error) {
  if (401 === error.response.status) {
      alert('intercepted a 401');
  } else {
      return Promise.reject(error);
  }
});

//tier config axios instance
const tc = {
  api: axios.create({
    baseURL: process.env.VUE_APP_API,
    headers: { "content-type": "application/json" },
    withCredentials: true
  }),
  URL_TIER_CONFIGURATION: "tier-configuration", //GET and POST
  URL_TIER_CONFIGURATION_ID: "tier-configuration/:id", //PUT
  URL_LOGS: "logs", //GET
  URL_LOGS_ID: "logs/:id", //GET with id
  URL_COUNTIES: "counties", //GET
  URL_POSTAL_CODES: "postal_codes", //GET
  URL_STATES: "states", //GET
  URL_VENDORS: "vendors", //GET
  URL_FORWARDERS: "forwarders" //GET
};

tc.api.interceptors.response.use(response => {
  return response;
}, error => {
  if (401 === error.response.status) {
    alert('Please login again.');
    window.location = process.env.VUE_APP_REDIRECT;
  } else {
      return Promise.reject(error);
  }
});

export default {
  /* 
  * Sample Usage in App
  * import tc from '../../api/tier-config'
  * 
  * tc.getCounties({ state: "nv"}, counties => {
  *      console.log(counties); //eslint-disable-line
  *    }, err => {
  *      console.log(err); //eslint-disable-line
  * });
  * 
  */
  getTierConfiguration(cb, errCb) {
    tc.api
      .get(tc.URL_TIER_CONFIGURATION)
      .then(response => {
        cb(response.data);
      })
      .catch(function(error) {
        errCb(error);
      });
  },
  saveTierConfigurations(data, cb, errCb) {
    tc.api.post(tc.URL_TIER_CONFIGURATION, data).then(response => {
      cb(response.data);
    })
    .catch(function(error) {
      errCb(error);
    });
  },
  updateTierConfigurations(id, data, cb, errCb) {
    //axios doesn't seem to have anything for path params, so there's no real clean way to do it
    //most people just seem to append
    const url = tc.URL_TIER_CONFIGURATION_ID.replace(":id",  id);
    tc.api.put(url, data).then(response => {
      cb(response.data);
    })
    .catch(function(error) {
      errCb(error);
    });
  },
  getCounties(params, cb, errCb) {
    //params: { state: "NV" }
    tc.api
      .get(tc.URL_COUNTIES, {params: params })
      .then(response => {
        cb(response.data);
      })
      .catch(function(error) {
        errCb(error);
      });
  },
  getZipCodes(params, cb, errCb) {
    //params: { state: "nv", county: "washoe" } - county requires state
    tc.api
      .get(tc.URL_POSTAL_CODES, {params: params })
      .then(response => {
        cb(response.data);
      })
      .catch(function(error) {
        errCb(error);
      });
  },
  getStates(cb, errCb) {
    tc.api
      .get(tc.URL_STATES)
      .then(response => {
        cb(response.data);
      })
      .catch(function(error) {
        errCb(error);
      });
  },
  getVendors(params, cb, errCb) {
    //params: { postal_code:"89521", state: "nv", county: "washoe" }
    //postal code and state are standalone params, county requires state
    tc.api
      .get(tc.URL_VENDORS, { params: params})
      .then(response => {
        cb(response.data);
      })
      .catch(function(error) {
        errCb(error);
      });
  },
  getForwarders(params, cb, errCb) {
    //params: { postal_code:"89521", state: "nv", county: "washoe" }
    //postal code and state are standalone params, county requires state
    tc.api
      .get(tc.URL_FORWARDERS, { params: params})
      .then(response => {
        cb(response.data);
      })
      .catch(function(error) {
        errCb(error);
      });
  },
  getHistory(cb, errCb){
    tc.api
      .get(tc.URL_LOGS)
      .then(response => {
        cb(response.data);
      })
      .catch(function(error) {
        errCb(error);
      });
  },
  getHistoryById(id, cb, errCb) {
    const url = tc.URL_LOGS_ID.replace(":id",  Number(id));
    tc.api.put(url, data).then(response => {
      cb(response.data);
    })
    .catch(function(error) {
      errCb(error);
    });
  },
};
