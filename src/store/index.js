import Vue from 'vue'
import Vuex from 'vuex'
import geo from './modules/geo'
import tiers from './modules/tiers'
import entities from './modules/entities'

//visit https://vuex.vuejs.org/guide/structure.html to see code structure recommendations
//https://vuex.vuejs.org/
Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    geo,
    tiers,
    entities
  },
  strict: debug
})