//geo module
export const SET_COUNTIES = '😆SET_COUNTIES'
export const SET_STATES = '🙃SET_STATES'
export const SET_ZIP_CODES = '😃SET_ZIP_CODES'

//entities module
export const SET_VENDORS = '😉SET_VENDORS'
export const SET_FORWARDERS = '😋SET_FORWARDERS'
export const INITIAL_LOAD_VENDORS = '🤪SET_INITIAL_VENDORS'
export const INITIAL_LOAD_FORWARDERS = '🤩SET_INITIAL_FORWARDERS'

//tiers module
export const LOAD_TIER_CONFIG = '😛LOAD_TIER_CONFIG'
export const SET_TIER_DIRTY = '😳SET_TIER_DIRTY'
export const SET_TIER_ALLOCS = '🥶SET_TIER_ALLOCS'
export const SET_TIER_NAME = '🤓SET_TIER_NAME'
export const SET_TIER_TRANSITION = '😇SET_TIER_TRANSITION'
export const REMOVE_TIER_ALLOCS = '🤡REMOVE_TIER_ALLOCS'
export const ADD_TIER = '🥳ADD_TIER'
export const COPY_TIER = '🤨COPY_TIER'
export const REMOVE_TIER = '😠REMOVE_TIER'
export const REMOVE_VENDOR = '🧐REMOVE_VENDOR'