import { SET_TIER_DIRTY, LOAD_TIER_CONFIG, SET_TIER_ALLOCS, SET_TIER_NAME, SET_TIER_TRANSITION, ADD_TIER, COPY_TIER, REMOVE_TIER, REMOVE_TIER_ALLOCS, REMOVE_VENDOR } from '../mutations'
import strings from '../../utils/strings'
import _states from '../modules/geodata/states.json'
import Ajv from 'ajv';
import schema from '../../utils/tier-config-schema.json'

var tc;
if (process.env.NODE_ENV === 'development'){
  tc = require('../../api/mocks/mock-tier-config').default
} else {
  tc = require('../../api/tier-config').default
}

const ajv = new Ajv();

const state = {
  configs: [],
  currentConfig: {},
  isDirty: 0 //using int, maybe make an undo stack someday
}

// getters
const getters = {
  getZipCodeRules: (state) => (params) => {
    let rules = [];
    if(params.tier && state.currentConfig?.tiers){
      state.currentConfig.tiers.forEach(t => {
        if(t.name === params.tier.name){
          if(t.allocation_weights?.postal_codes){
            Object.keys(t.allocation_weights.postal_codes).forEach(zipCode => {
              rules.push({ tier: params.tier, name: zipCode, type: strings.ALLOCATION_CARD_TYPE.ZIP })
            })
          }
        }
      });
    }
    return rules;
  },
  getVendorByUdbsaid:(state, getters, rootState) => (udbsaid) => {
    let f = rootState.entities.forwarders.find(f => f.udbsaid === udbsaid);
    if(f){ //look for a forwarder with this udbsaid
      return {name: f.name, udbsaid: udbsaid, type: strings.ENTITY_TYPE.FORWARDER, allocations: [], tiersPresent: {}, activeCard: false};
    }
    if(!f){ //look for a direct agent
      let d = rootState.entities.vendors.find(d => d.udbsaid === udbsaid);
      if(d){
        return {name: d.name, udbsaid: udbsaid, type: strings.ENTITY_TYPE.VENDOR, allocations: [], tiersPresent: {}, activeCard: false}
      } 
    }
    return null;
  },
  getVendorAllocations:(state, getters) => () => { 
    let vendors = [];
    if(state.currentConfig?.tiers){
      state.currentConfig.tiers.forEach(tier => {
        if(tier.allocation_weights?.national){
          Object.keys(tier.allocation_weights.national).forEach(udbsaid => {
            //national level should only be forwarders
            let v = vendors.find(v => v.udbsaid === udbsaid)
            if(!v) { //don't already have an entry for him in allocated vendors array
              v = getters.getVendorByUdbsaid(udbsaid);
              if(v) { vendors.push(v); }
            }
            if(v){//okay entry exists or is created and set, add this allocation
              v.tiersPresent[tier.name] = tier.ordinal;
              v.allocations.push({ tier: {name: tier.name, ordinal: tier.ordinal }, //building the same model as an AllocationCard 
                activeCard: true, name:"United States", abbreviation: "USA",
                type: strings.ALLOCATION_CARD_TYPE.NATION, highlight:udbsaid,
                amount: tier.allocation_weights.national[udbsaid].toString() })
            } //else -> ??? something wonky happened, no udbsaid matching, maybe database entry got deleted, ignore entry
          })
        }

        if(tier.allocation_weights?.postal_codes){
          Object.keys(tier.allocation_weights.postal_codes).forEach(zip => {
            Object.keys(tier.allocation_weights.postal_codes[zip]).forEach(udbsaid => {
              let v = vendors.find(v => v.udbsaid === udbsaid)
              if(!v) { //don't already have an entry for him in allocated vendors array
                v = getters.getVendorByUdbsaid(udbsaid);
                if(v) { vendors.push(v); }
              }
              if(v){//okay entry exists or is created and set, add this allocation
                v.tiersPresent[tier.name] = tier.ordinal;
                v.allocations.push({ tier: {name: tier.name, ordinal: tier.ordinal }, //building the same model as an AllocationCard 
                  activeCard: true, name:zip, abbreviation: "",
                  type: strings.ALLOCATION_CARD_TYPE.ZIP, highlight:udbsaid,
                  amount: tier.allocation_weights.postal_codes[zip][udbsaid].toString() })
              } //else -> ??? something wonky happened, no udbsaid matching, maybe database entry got deleted, ignore entry
            })
          })
        }

        if(tier.allocation_weights?.states){
          Object.keys(tier.allocation_weights.states).forEach(abbreviation => {
            Object.keys(tier.allocation_weights.states[abbreviation]).forEach(type => {
              
              if(type === "state"){
                Object.keys(tier.allocation_weights?.states[abbreviation].state).forEach(udbsaid => {
                  let v = vendors.find(v => v.udbsaid === udbsaid)
                  if(!v) { //don't already have an entry for him in allocated vendors array
                    v = getters.getVendorByUdbsaid(udbsaid);
                    if(v) { vendors.push(v); }
                  }
                  if(v){//okay entry exists or is created and set, add this allocation
                    v.tiersPresent[tier.name] = tier.ordinal;
                    v.allocations.push({ tier: {name: tier.name, ordinal: tier.ordinal }, //building the same model as an AllocationCard 
                      activeCard: true, abbreviation:abbreviation,
                      name: _states.find(s => s.abbreviation === abbreviation).name,
                      type: strings.ALLOCATION_CARD_TYPE.STATE, highlight:udbsaid,
                      amount: tier.allocation_weights.states[abbreviation].state[udbsaid].toString() })
                  } //else -> ??? something wonky happened, no udbsaid matching, maybe database entry got deleted, ignore entry
                })
              }
              
              if(type === "counties"){
                Object.keys(tier.allocation_weights.states[abbreviation].counties).forEach(county => {
                  Object.keys(tier.allocation_weights.states[abbreviation].counties[county]).forEach(udbsaid => {
                    let v = vendors.find(v => v.udbsaid === udbsaid)
                    if(!v) { //don't already have an entry for him in allocated vendors array
                      v = getters.getVendorByUdbsaid(udbsaid);
                      if(v) { vendors.push(v); }
                    }
                    if(v){//okay entry exists or is created and set, add this allocation
                      v.tiersPresent[tier.name] = tier.ordinal;
                      v.allocations.push({ tier: {name: tier.name, ordinal: tier.ordinal }, //building the same model as an AllocationCard 
                        activeCard: true, abbreviation: abbreviation,
                        name: county,
                        type: strings.ALLOCATION_CARD_TYPE.COUNTY, highlight:udbsaid,
                        amount: tier.allocation_weights.states[abbreviation].counties[county][udbsaid].toString() })
                    } //else -> ??? something wonky happened, no udbsaid matching, maybe database entry got deleted, ignore entry
                  })
                })
              }
            })
          })
        }
      })
    }
    return vendors.length > 0 ? vendors : [{name: 'No Allocations Found...', udbsaid: '-1', type: 'V', allocations: [], activeCard: false}];
  },
  getAllocationRule: (state, getters, rootState) => (params) => {
    var allocations = [{udbsaid:'err!', name: "No Rules", amount:0 }];
    let loadedParentRules = false;
    if(params.tier && state.currentConfig?.tiers) {
      state.currentConfig.tiers.forEach(t => {
        if(t.name === params.tier.name){
          //found the active tier
          switch(params.type){
            //now look for the allocation rule type
            //alloc amounts are used as strings (toString()) for simple comparison and binding purposes on the UI and are converted back to numbers on save
            case strings.ALLOCATION_CARD_TYPE.NATION:
              if(t.allocation_weights?.national){
                allocations = [];
                Object.keys(t.allocation_weights.national).forEach(ubdsaid => {
                  //national allocs are only using forwarders
                  let f = rootState.entities.forwarders.find(f => f.udbsaid === ubdsaid);
                  if(f) { allocations.push({ udbsaid: f.udbsaid, name: f.name, 
                    amount: t.allocation_weights.national[f.udbsaid].toString(), type: strings.ENTITY_TYPE.FORWARDER }) }
                })
                loadedParentRules = true; //force true on this to hide pin on nation card so it can't be reset
              } //if no national alloc rules are found, then the initial array with 'err!' will return
              break;
            case strings.ALLOCATION_CARD_TYPE.STATE:
              //if there is a state rule for the exact state in this tier
              if(t.allocation_weights?.states && t.allocation_weights?.states[params.abbreviation]?.state){
                allocations = [];
                Object.keys(t.allocation_weights.states[params.abbreviation].state).forEach(ubdsaid => {
                  //national allocs are only using forwarders
                  let f = rootState.entities.forwarders.find(f => f.udbsaid === ubdsaid);
                  if(f) { allocations.push({ udbsaid: f.udbsaid, name: f.name, 
                    amount: t.allocation_weights.states[params.abbreviation].state[f.udbsaid].toString(), type: strings.ENTITY_TYPE.FORWARDER }) 
                  }
                  else {
                    let v = rootState.entities.vendors.find(v => v.udbsaid === ubdsaid);
                    if (v) {
                      allocations.push({ udbsaid: v.udbsaid, name: v.name,
                        amount: t.allocation_weights.states[params.abbreviation].state[v.udbsaid].toString(), type: strings.ENTITY_TYPE.VENDOR})
                    }
                  }
                })
                //if (allocations.length > 0) { allocations.push(allocLoaded); }
              } else { //recursively get the 'parent' national rule
                //make sure not to call return getters.getAllocationRule(... etc), we're in a foreach
                loadedParentRules = true;
                allocations = getters.getAllocationRule({ tier: params.tier, name: "United States", abbreviation:"USA", type: strings.ALLOCATION_CARD_TYPE.NATION }).allocations
              }
              break;
            case strings.ALLOCATION_CARD_TYPE.COUNTY:
              //if there is a county rule for the exact county and state in this tier
              if(t.allocation_weights?.states && t.allocation_weights?.states[params.abbreviation]?.counties
                  && t.allocation_weights?.states[params.abbreviation]?.counties[params.name]){
                allocations = [];
                Object.keys(t.allocation_weights.states[params.abbreviation].counties[params.name]).forEach(ubdsaid => {
                  //national allocs are only using forwarders
                  let f = rootState.entities.forwarders.find(f => f.udbsaid === ubdsaid);
                  if(f) { allocations.push({ udbsaid: f.udbsaid, name: f.name, 
                    amount: t.allocation_weights.states[params.abbreviation].counties[params.name][f.udbsaid].toString(), type: strings.ENTITY_TYPE.FORWARDER }) 
                  }
                  else {
                    let v = rootState.entities.vendors.find(v => v.udbsaid === ubdsaid);
                    if (v) {
                      allocations.push({ udbsaid: v.udbsaid, name: v.name,
                        amount: t.allocation_weights.states[params.abbreviation].counties[params.name][v.udbsaid].toString(), type: strings.ENTITY_TYPE.VENDOR})
                    }
                  }
                })
              } else { //recursively get the 'parent' state rule
                //make sure not to call return getters.getAllocationRule(... etc), we're in a foreach
                loadedParentRules = true;
                allocations = getters.getAllocationRule({ tier: params.tier, name: "unimportantOnlyAbbrIsUsed", abbreviation:params.abbreviation, type: strings.ALLOCATION_CARD_TYPE.STATE }).allocations
              }
              break;
            default: //treat as zip card
              //if there is a postal_code rule for the exact county and state in this tier
              if(t.allocation_weights?.postal_codes && t.allocation_weights?.postal_codes[params.name]){
                allocations = [];
                Object.keys(t.allocation_weights.postal_codes[params.name]).forEach(ubdsaid => {
                  //national allocs are only using forwarders
                  let f = rootState.entities.forwarders.find(f => f.udbsaid === ubdsaid);
                  if(f) { allocations.push({ udbsaid: f.udbsaid, name: f.name, 
                    amount: t.allocation_weights.postal_codes[params.name][f.udbsaid].toString(), type: strings.ENTITY_TYPE.FORWARDER }) 
                  }
                  else {
                    let v = rootState.entities.vendors.find(v => v.udbsaid === ubdsaid);
                    if (v) {
                      allocations.push({ udbsaid: v.udbsaid, name: v.name,
                        amount: t.allocation_weights.postal_codes[params.name][v.udbsaid].toString(), type: strings.ENTITY_TYPE.VENDOR})
                    }
                  }
                })
              } else { //recursively get the 'parent' state rule
                loadedParentRules = true;
                allocations = params.countyName ?
                  getters.getAllocationRule({ tier: params.tier, name: params.countyName, abbreviation:params.abbreviation, type: strings.ALLOCATION_CARD_TYPE.COUNTY }).allocations : 
                    params.abbreviation ?
                      getters.getAllocationRule({ tier: params.tier, name: "fallBackToStateRule", abbreviation:params.abbreviation, type: strings.ALLOCATION_CARD_TYPE.STATE }).allocations :
                      [] //for exact zip code searches, we won't know zip or state, and can just return an empty allocation if no prior zip code rule exists
              }
              break;
          }
        }
      });
    }
    return { allocations: allocations, loadedParentRules: loadedParentRules };
  },
  createCurrentConfigLocalCopy: (state) => () => {
    //vuex gets angry with errors when manipulating the state, so clone the current config to be used as a
    //local object by the view that needs it, then commit it back when object is destroyed
    //you would think they would automatically make getters clone on your behalf if this is the pattern they want...
    return JSON.parse(JSON.stringify(state.currentConfig));
  }
}

// actions  
const actions = {
  getTierConfiguration ({ commit, state }, forceRefresh) {
    //https://forum.vuejs.org/t/getters-do-not-react-to-mutations-from-action/11943/13
    //note that getters don't react to mutations from action - best to use promises it seems
    //as Vue.set(state, x, xvalue) isn't reactive on a child component already computed
    return new Promise((resolve, reject) => {
      if(forceRefresh || state.configs.length === 0){
        tc.getTierConfigurations(configs => {
          commit(LOAD_TIER_CONFIG, configs );
          resolve(true);
        }, err => { 
          this._vm.$toasted.show('Could not retrieve Tier information', { duration : 3000,  type: 'error' }); 
          reject(err);
        })
      } else {
        resolve(true);
      }
    });
  },
  //This is the primary saved call being used in the MVP of tiering/allocation
  saveNewTierConfiguration ({ commit }, jsonString) { //POST call - this creates a new config with a new id generated by the server
    return new Promise((resolve, reject) => {
      var config = JSON.parse(jsonString);  
      var valid = ajv.validate(schema, config);
      console.log(config);//eslint-disable-line
      if (!valid) {
        let transition_err = false;
        ajv.errors.map(err => {
          let paths = err.dataPath.split('.');
          let tierNum = Number(paths[1].replace('tiers[','').replace(']',''))+1;
          let errField = paths[paths.length-1];
          if (errField === 'transition_criteria'){
            if(!transition_err){//show show this once and not spam screen
              this._vm.$toasted.show(`<br>Tier ${tierNum} must have a valid Duration or Charge off Setting.<br><br>`, { duration : 8000,  type: 'error' });  
              transition_err = true;
            }
          } else if (err.message.indexOf('national') > -1){
              this._vm.$toasted.show(`<br>Tier ${tierNum} is missing allocations at the national level.<br><br>`, { duration : 8000,  type: 'error' });  
          } else {
            this._vm.$toasted.show(`Err: Tier ${tierNum} ${errField} ${err.message}`, { duration : 8000,  type: 'error' });  
          }
        });
        reject(ajv.errors);
        return;
      }

      //check for a post charge off flag and fire a warning
      let postChargeOffSet = false;
      config.tiers.forEach(t => {
        if(t.transition_criteria.case_extra?.post_charge_off){
          postChargeOffSet = true;
        }
      })
      //TODO check for a permissions thing in the future and definitely don't show this if post charge off is not valid for the client
      if(!postChargeOffSet){
        this._vm.$toasted.show(`<br>Your configuration was updated, but we just wanted to let you know that none of your tiers have a post charge off setting.<br><br>`, { duration : 8000,  type: 'info' });  
      }

      tc.saveTierConfigurations(config, res => {
        this._vm.$toasted.show('Configuration Saved!', { duration : 3000,  type: 'success' });
        commit(SET_TIER_DIRTY, false);
        resolve(res);
      }, err => { 
        this._vm.$toasted.show('Could not update Tier information', { duration : 3000,  type: 'error' });
        reject(err); 
      })
    });
  },
  /* IMPORTANT - WE'RE CURRENTLY NOT USING THIS CALL... EVERY SET IS IMMUTABLE AND WE SAVE A NEW CONFIG ID USING THE ABOVE CALL */
  updateTierConfiguration ({ commit }, id, jsonString) { //PUT call - this overwrites current tier config using the id
    return new Promise((resolve, reject) => {
      if(!id || id.length < 10){
        this._vm.$toasted.show(`invalid configuration id`, { duration : 5000,  type: 'error' });
        reject('json schema validation failed');
        return;
      }
      var config = JSON.parse(jsonString);  
      var valid = ajv.validate(schema, config);
      if (!valid) {
        ajv.errors.map(err => {
          this._vm.$toasted.show(`${err.dataPath} ${err.message}`, { duration : 5000,  type: 'error' });  
        });
        reject(ajv.errors);
        return;
      }
      tc.updateTierConfigurations(id, config, res => {
        this._vm.$toasted.show('Configuration Updated!', { duration : 3000,  type: 'success' });
        commit(SET_TIER_DIRTY, false);
        resolve(res);
      }, err => { 
        this._vm.$toasted.show('Could not retrieve Tier information', { duration : 3000,  type: 'error' }); 
        reject(err);
      })
    });
  },
  getHistory () {
    return new Promise((resolve, reject) => {
      tc.getHistory(data => {
        resolve(data);
      }, err => { 
        this._vm.$toasted.show('Could not retrieve History Information', { duration : 3000,  type: 'error' });
        reject(err); 
      })
    });
  },
  getHistoryById ({}, id) { //eslint-disable-line
    return new Promise((resolve, reject) => {
      tc.getHistoryById(id, data => {
        resolve(data);
      }, err => { 
        this._vm.$toasted.show('Could not retrieve History Information', { duration : 3000,  type: 'error' });
        reject(err); 
      })
    });
  }
}

// mutations
const mutations = {
  [LOAD_TIER_CONFIG] (state, configs) {
    this._vm.$set(state, 'configs', configs);
    let curr = configs.find(c => c.is_current === true) || {};
    this._vm.$set(state, 'currentConfig', curr);
    state.isDirty = 0;
  },
  [REMOVE_TIER] (state, tierName) {
    if(state.currentConfig?.tiers){
      state.currentConfig.tiers.splice(state.currentConfig.tiers.findIndex(t=>t.name === tierName), 1);
      state.currentConfig.tiers.forEach((t,idx) => t.ordinal = idx+1);
      state.isDirty++;
    }
  },
  [SET_TIER_DIRTY] (state, dirty) {
    state.isDirty = dirty;
  },
  [ADD_TIER] (state, tier) {
    if(state.currentConfig?.tiers){
      state.currentConfig.tiers.push({...tier});
    } else {
      state.currentConfig.tiers = [...tier];
    }
    state.isDirty++;
  },
  [COPY_TIER] (state, data) {
    state.currentConfig.tiers.forEach(t => {
      if(t.name === data.copy){
        data.tier.allocation_weights = { ...t.allocation_weights};
        state.currentConfig.tiers.push({...data.tier});
        state.isDirty++;
      }
    })
  },
  [REMOVE_VENDOR] (state, data) {
    state.currentConfig.tiers.forEach(t => {
      if([strings.ALLOCATION_CARD_TYPE.TOTAL, strings.ALLOCATION_CARD_TYPE.NATION].includes(data.type)){
        //additional if check needed
        if(t.allocation_weights?.national && t.allocation_weights?.national[data.udbsaid]){
          delete t.allocation_weights?.national[data.udbsaid];
          Object.keys(t.allocation_weights.national).length > 0 ? 
            redistributeAllocations(t.allocation_weights?.national) : delete t.allocation_weights?.national;
        }
      }
      if([strings.ALLOCATION_CARD_TYPE.TOTAL, strings.ALLOCATION_CARD_TYPE.STATE].includes(data.type)){
        if(t.allocation_weights?.states){
          Object.keys(t.allocation_weights.states).forEach(stateAbbr => {
            if(t.allocation_weights.states[stateAbbr]?.state[data.udbsaid]){
              delete t.allocation_weights.states[stateAbbr]?.state[data.udbsaid];
              Object.keys(t.allocation_weights?.states[stateAbbr].state).length > 0 ? 
                redistributeAllocations(t.allocation_weights?.states[stateAbbr].state) : delete t.allocation_weights?.states[stateAbbr].state;
            }
          });
        }
      }
      if([strings.ALLOCATION_CARD_TYPE.TOTAL, strings.ALLOCATION_CARD_TYPE.COUNTY].includes(data.type)){
        if(t.allocation_weights?.states){
          Object.keys(t.allocation_weights.states).forEach(stateAbbr => {
            if(t.allocation_weights.states[stateAbbr]?.counties){
              Object.keys(t.allocation_weights.states[stateAbbr].counties).forEach(county => {
                if(t.allocation_weights.states[stateAbbr]?.counties[county][data.udbsaid]){
                  delete t.allocation_weights.states[stateAbbr]?.counties[county][data.udbsaid];
                  Object.keys(t.allocation_weights.states[stateAbbr].counties[county]).length > 0 ? 
                    redistributeAllocations(t.allocation_weights.states[stateAbbr]?.counties[county]) : delete t.allocation_weights.states[stateAbbr]?.counties[county];
                }
              })
            }
          });
        }
      }
      if([strings.ALLOCATION_CARD_TYPE.TOTAL, strings.ALLOCATION_CARD_TYPE.ZIP].includes(data.type)){
        if(t.allocation_weights?.postal_codes){
          Object.keys(t.allocation_weights.postal_codes).forEach(zip => {
            if(t.allocation_weights.postal_codes[zip][data.udbsaid]){
              delete t.allocation_weights.postal_codes[zip][data.udbsaid];
              Object.keys(t.allocation_weights.postal_codes[zip]).length > 0 ? 
                redistributeAllocations(t.allocation_weights.postal_codes[zip]) : delete t.allocation_weights.postal_codes[zip];
            }
          });
        }
      }
    })
    state.isDirty++;
  },
  [REMOVE_TIER_ALLOCS] (state, data) {
    state.currentConfig.tiers.forEach(t => {
      if(t.name === data.tier.name){
        switch(data.type){
          case strings.ALLOCATION_CARD_TYPE.STATE:
            if(t.allocation_weights?.states && t.allocation_weights?.states[data.abbreviation]?.state){
              delete t.allocation_weights.states[data.abbreviation].state;
            }
            break;
          case strings.ALLOCATION_CARD_TYPE.COUNTY:
            if(t.allocation_weights?.states && t.allocation_weights?.states[data.abbreviation]?.counties
              && t.allocation_weights?.states[data.abbreviation]?.counties[data.name]){
                delete t.allocation_weights.states[data.abbreviation].counties[data.name];
            }
            break;
          default: //zip card
            if(t.allocation_weights?.postal_codes && t.allocation_weights?.postal_codes[data.name]){
              delete t.allocation_weights.postal_codes[data.name];
            }
            break;     
        }
        state.isDirty++;
      }
    })
  },
  [SET_TIER_ALLOCS] (state, data) {
    let tier = data.tier;
    if(tier){
      if(state.currentConfig?.tiers){
        state.currentConfig.tiers.forEach(t => {
          if(t.name === tier.name){
            if(t.allocation_weights){
              let alloc = {}
              data.allocations.forEach(a => alloc[a.udbsaid] = Number(a.amount) )
              switch(data.type){
                case strings.ALLOCATION_CARD_TYPE.NATION:
                  this._vm.$set(t.allocation_weights, 'national', alloc);
                  break;
                case strings.ALLOCATION_CARD_TYPE.STATE:
                  if(!t.allocation_weights.states) {
                    t.allocation_weights.states = {};
                  }
                  if(!t.allocation_weights.states[data.abbreviation]){
                    t.allocation_weights.states[data.abbreviation] = {};
                  }
                  this._vm.$set(t.allocation_weights.states[data.abbreviation], 'state', alloc);
                  break;
                case strings.ALLOCATION_CARD_TYPE.COUNTY:
                    if(!t.allocation_weights.states) {
                      t.allocation_weights.states = {};
                    }
                    if(!t.allocation_weights.states[data.abbreviation]){
                      t.allocation_weights.states[data.abbreviation] = {};
                    }
                    if(!t.allocation_weights.states[data.abbreviation].counties){
                      t.allocation_weights.states[data.abbreviation].counties = {};
                    }
                    this._vm.$set(t.allocation_weights.states[data.abbreviation].counties, data.name, alloc);
                    break;
                default: //zip card
                    if(!t.allocation_weights.postal_codes) {
                      t.allocation_weights.postal_codes = {};
                    }
                    this._vm.$set(t.allocation_weights.postal_codes, data.name, alloc);
                    break;     
              }
            }
            state.isDirty++;
          }
        });
      }
    }
  },
  [SET_TIER_NAME] (state, data) {
    let tier = data.tier;
    if(tier){
      if(state.currentConfig?.tiers){
        state.currentConfig.tiers.forEach(t => {
          if(t.name === tier.name){
            t.name = data.name;
          }
        });
      }
      state.isDirty++;
    }
  },
  [SET_TIER_TRANSITION] (state, data) {
    let tier = JSON.parse(JSON.stringify(data.tier));
    if(tier){
      if(state.currentConfig?.tiers){
        state.currentConfig.tiers.forEach(t => {
          if(t.name === tier.name){
            t.transition_criteria = { ...tier.transition_criteria };
            if(t.transition_criteria?.case_extra?.post_charge_off){//correct issue with ui binding turning it to string
              delete t.transition_criteria?.time;
            } else if(t.transition_criteria?.time) {
              t.transition_criteria.time.days = Number(t.transition_criteria.time.days);
              delete t.transition_criteria?.case_extras;
            }
          }
        });
      }
      state.isDirty++;
    }
  },
}

function redistributeAllocations(allocation){//eslint-disable-line
  let sum = Object.values(allocation).reduce((total, val) => {return total + Number(val)}, 0);
  Object.keys(allocation).forEach(k => allocation[k] = Math.floor((Number(allocation[k])/sum)*(100 - sum) + Number(allocation[k])));
  let remainder = 100 - Object.values(allocation).reduce((total, val) => {return total + Number(val)}, 0);
  allocation[Object.keys(allocation)[0]]= Number(allocation[Object.keys(allocation)[0]]) + remainder;
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}