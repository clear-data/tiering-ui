import { SET_VENDORS, SET_FORWARDERS, INITIAL_LOAD_VENDORS, INITIAL_LOAD_FORWARDERS } from '../mutations'

var tc;
if (process.env.NODE_ENV === 'development'){
  tc = require('../../api/mocks/mock-tier-config').default
} else {
  tc = require('../../api/tier-config').default
}

// state tree - https://vuex.vuejs.org/guide/state.html
const state = {
  // store forwarders and vendors (agents) plus their area of service for STATE and COUNTY only
  // for zip codes we will just query ad hoc and use the response
  forwarders: [],
  vendors: [],
  queried_forwarders: {},
  queried_vendors: {}
}

// getters - https://vuex.vuejs.org/guide/getters.html
// don't use getters for simple things like forwarders: state => state.forwarders, components can use ...mapState for that
const getters = {
    forwardersByStateAbbr: (state) => (abbr) => {
      return state.forwarders.filter(forwarder => forwarder.states.includes(abbr.toLowerCase()))
    },
    forwardersByCountyStateAbbr: (state) => (county, abbr) => {
      return state.forwarders.filter(forwarder => forwarder.counties.includes((abbr+county).toLowerCase()))
    },
    vendorsByStateAbbr: (state) => (abbr) => {
      return state.vendors.filter(vendors => vendors.states.includes(abbr.toLowerCase()))
    },
    vendorsByCountyStateAbbr: (state) => (county, abbr) => {
      return state.vendors.filter(vendors => vendors.counties.includes((abbr+county).toLowerCase()))
    },
}

/* actions call mutations - meant for asynchonous state handling
* https://vuex.vuejs.org/guide/actions.html
* with vendors and forwarders, the api returns just udbsaid and name for the requested state,county,zip
* so when the requests are made, inject the params that were used so we can cache and query locally
*/
/*while these promises are being rejected on error, there's no real need to handle the rejections in the components
the calls here throw a toast before rejecting... not much the UI can do if this information can't be retrieved*/
const actions = {
  getInitialApprovedVendorsForwarders({commit, state}){
    return new Promise((resolve, reject) => {
      if(state.vendors.length === 0){  
        tc.getVendors({}, data => {
          commit(INITIAL_LOAD_VENDORS, data.vendors);
        }, err => { 
          this._vm.$toasted.show('Could not retrieve Vendor information', { duration : 3000,  type: 'error' }); 
          reject(err);
        })  
      }
      //resolve the promise in forwarder fetch and not vendor fetch, the vendors call is more of a prefetch for them
      //UI needs forwarder info first for national rule display
      if(state.forwarders.length === 0){  
        tc.getForwarders({}, data => {
          commit(INITIAL_LOAD_FORWARDERS, data.forwarders);
          resolve();
        }, err => { 
          this._vm.$toasted.show('Could not retrieve Forwarder information', { duration : 3000,  type: 'error' }); 
          reject(err);
        })
      } else {
        resolve();
      }
    });
  },
  getForwardersByStateAbbr ({ commit, state }, params) {
    return new Promise((resolve, reject) => {
      const key = (params.state).toLowerCase();
      if (!state.queried_forwarders[key]){
        if (key === "usa") {
          params = {} //this will send empty params to config service and query forwarders that cover the entire US
        }
        tc.getForwarders(params, data => {
          commit(SET_FORWARDERS, { forwarders: data.forwarders, key: key, query: 'states'});
          resolve();
        }, err => { 
          this._vm.$toasted.show('Could not retrieve Forwarder information - State', { duration : 3000,  type: 'error' }); 
          reject(err);
        })
      } else {
        resolve();
      }
    });
  },
  getForwardersByCountyStateAbbr ({ commit, state }, params) {
    return new Promise((resolve, reject) => {
      const key = (params.state+params.county).toLowerCase();
      if (!state.queried_forwarders[key]){
        tc.getForwarders(params, data => {
          commit(SET_FORWARDERS, { forwarders: data.forwarders, key: key, query: 'counties'});
          resolve();
        }, err => { 
          this._vm.$toasted.show('Could not retrieve Forwarder information - County', { duration : 3000,  type: 'error' }); 
          reject(err);
        })
      } else {
        resolve();
      }
    });
  },
  //not going to cache zipcodes, returning and setting in UI through async call
  getForwardersByZipCode ({}, params) { //eslint-disable-line
    return new Promise((resolve, reject) => {
      tc.getForwarders(params, data => {
        resolve(data.forwarders);
      }, err => { 
        this._vm.$toasted.show('Could not retrieve Forwarder information - Zip Code', { duration : 3000,  type: 'error' }); 
        reject(err);
      })
    });
  },   
  getVendorsByStateAbbr ({ commit, state }, params) {
    return new Promise((resolve, reject) => {
      const key = (params.state).toLowerCase();
      if (!state.queried_vendors[key]){
        tc.getVendors(params, data => {
            commit(SET_VENDORS, { vendors: data.vendors, key: key, query: 'states'});
            resolve();
        }, err => { 
          this._vm.$toasted.show('Could not retrieve Vendor information - States', { duration : 3000,  type: 'error' }); 
          reject(err);
        })
      } else {
        resolve();
      }
    });
  },
  getVendorsByCountyStateAbbr ({ commit, state }, params) {
    return new Promise((resolve, reject) => {
      const key = (params.state+params.county).toLowerCase();
      if (!state.queried_vendors[key]){
        tc.getVendors(params, data => {
            commit(SET_VENDORS, { vendors: data.vendors, key: key, query: 'counties'});
            resolve();
        }, err => {  
          this._vm.$toasted.show('Could not retrieve Vendor information - Counties', { duration : 3000,  type: 'error' }); 
          reject(err);
        })
      } else {
        resolve();
      }
    });
  },
  //not going to cache zipcodes, returning and setting in UI through async call
  getVendorsByZipCode ({}, params) { //eslint-disable-line
    return new Promise((resolve, reject) => {
      tc.getVendors(params, data => {
        resolve(data.vendors);
      }, err => { 
        this._vm.$toasted.show('Could not retrieve Vendor information - Zip Code', { duration : 3000,  type: 'error' });
        reject(err);
       })
    });
  },
}

// mutations - must be synchronous https://vuex.vuejs.org/guide/mutations.html
const mutations = {
  [INITIAL_LOAD_FORWARDERS] (state, res){
    let forwarders = [];
    res.forEach(f => { forwarders.push({ ...f, states: [], counties: [] }) });
    this._vm.$set(state, 'forwarders', forwarders);
    //this._vm.$toasted.show('Initial Forwarders Loaded', { duration : 3000,  type: 'success' });
  },
  [INITIAL_LOAD_VENDORS] (state, res){
    let vendors = [];
    res.forEach(v => { vendors.push({ ...v, states: [], counties: [] }) });
    this._vm.$set(state, 'vendors', vendors);
    //this._vm.$toasted.show('Initial Vendors Loaded', { duration : 3000,  type: 'success' });
  },
  [SET_FORWARDERS] (state, res) {
    res.forwarders.forEach(forwarder => {
        let idx = state.forwarders.findIndex(f => f.udbsaid === forwarder.udbsaid)
        if (idx > -1) { //update existing forwarder entry with new service state/county
          state.forwarders[idx][res.query].push(res.key);
        } else { //this forwarder doesn't exist in the state array yet
          let f = { ...forwarder, states: [], counties: [] }
          f[res.query].push(res.key);
          state.forwarders.push(f);
        }  
    })
    //using watchers in the components, need to use set for reactivity
    //https://vuejs.org/v2/guide/reactivity.html
    this._vm.$set(state.forwarders, 'forwarders', state.forwarders);
    state.queried_forwarders[res.key] = true;
    //console.log(state.forwarders);//eslint-disable-line
    //console.log(state.queried_forwarders);//eslint-disable-line
    
  },
  [SET_VENDORS] (state, res) {
    res.vendors.forEach(vendor => {
      let idx = state.vendors.findIndex(v => v.udbsaid === vendor.udbsaid)
      if (idx > -1) { //update existing vendor entry with new service state/county
        state.vendors[idx][res.query].push(res.key);
      } else { //this forwarder doesn't exist in the state array yet
        let v = { ...vendor, states: [], counties: [] }
        v[res.query].push(res.key);
        state.vendors.push(v);
      }  
    })
    this._vm.$set(state, 'vendors', state.vendors);
    state.queried_vendors[res.key] = true;
    //console.log(state.vendors);//eslint-disable-line
    //console.log(state.queried_vendors);//eslint-disable-line
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}