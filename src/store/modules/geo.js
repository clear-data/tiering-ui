import { SET_COUNTIES, SET_STATES, SET_ZIP_CODES } from '../mutations'
import _states from '../modules/geodata/states.json'
import _counties from '../modules/geodata/counties.json'

var tc;
if (process.env.NODE_ENV === 'development'){
  tc = require('../../api/mocks/mock-tier-config').default
} else {
  tc = require('../../api/tier-config').default
}
//state tree - https://vuex.vuejs.org/guide/state.html
const state = {
  //store states, county - json files are from the server
  states: _states,
  counties: _counties,
  //store zipcode information as they're retrieved
  zip_codes: {}
}

// getters - https://vuex.vuejs.org/guide/getters.html
// don't use getters for simple things like states: state => state.states, components can use ...mapState for that
const getters = {
    countiesByStateAbbr: (state) => (abbr) => {
      return state.counties[abbr];
    },
    zipsByCountyStateAbbr: (state) => (county, abbr) => {
      return state.zip_codes[(abbr+county).toLowerCase()];
    }
}

// actions call mutations - meant for asynchonous state handling
// https://vuex.vuejs.org/guide/actions.html
const actions = {
  getStates ({ commit }) {
    tc.getStates(states => {
        commit(SET_STATES, states);
    }, () => { this._vm.$toasted.show('Could not retrieve States information', { duration : 3000,  type: 'error' }); })
  }, 
  getCounties ({ commit }) {
    tc.getCounties({}, counties => {
      commit(SET_COUNTIES, counties);
    }, () => { this._vm.$toasted.show('Could not retrieve County Information', { duration : 3000,  type: 'error' }); })
  },
  getZipCodesByCounty ({ commit, state }, params) {
    return new Promise((resolve, reject) => {
      //params are { state: "nv", county: "washoe" }
      const key = (params.state+params.county).toLowerCase();
      if (!state.zip_codes[key]){
        tc.getZipCodes(params, data => {
          commit(SET_ZIP_CODES, { zip_codes: data.postal_codes, key: key} );
          resolve();
        }, err => { 
          this._vm.$toasted.show('Could not retrieve Zip Codes for ' + params.county + ', ' + params.state, { duration : 3000,  type: 'error' });
          reject(err); 
        })
      } else {
        resolve();
      }
    });
  }
}

// mutations - must be synchronous https://vuex.vuejs.org/guide/mutations.html
const mutations = {
  [SET_COUNTIES] (state, counties) {
    state.counties = counties
  },
  [SET_STATES] (state, states) {
    //since these are seeded, we don't need the $set convention
    state.states = states
  },
  [SET_ZIP_CODES] (state, res) {
    //using watchers in the components, need to use set for reactivity
    //https://vuejs.org/v2/guide/reactivity.html
    this._vm.$set(state.zip_codes, res.key, res.zip_codes);
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}