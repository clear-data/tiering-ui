#test docker image locally
FROM nginx:alpine
COPY ./dist/ /var/www
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
