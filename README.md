# tiering-ui

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Docker test build
```
sudo docker build -t tiering-ui:0.0.1 .
docker run -d -p 80:80 tiering-ui:0.0.1
```

### Docker test aws push - non-prod
```
$(aws ecr get-login --no-include-email --region us-east-2)
docker tag tiering-ui:0.0.1 415115050499.dkr.ecr.us-east-2.amazonaws.com/tiering-ui:0.0.1
docker push 415115050499.dkr.ecr.us-east-2.amazonaws.com/tiering-ui:0.0.1
```